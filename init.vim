set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
se directory=$HOME/.vim/swap//

call plug#begin()
    " Plug 'file://'.expand('/mnt/c/Users/kir/Projects/ast-editor/plug.vim')
    " Plug 'peach-lasagna/hello.nvim'
    Plug 'tpope/vim-surround'
    " Plug 'chrisbra/csv.vim'

    "Plug 'morhetz/gruvbox'
    " Plug 'joshdick/onedark.vim'
    Plug 'tomasiser/vim-code-dark'

    Plug 'glepnir/galaxyline.nvim'
    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'romgrk/barbar.nvim'
    Plug 'kyazdani42/nvim-tree.lua'
    Plug 'liuchengxu/vista.vim'
    Plug 'simrat39/rust-tools.nvim'

    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    " Plug 'notomo/gesture.nvim'

    Plug 'airblade/vim-gitgutter'
    " Plug 'lewis6991/gitsigns.nvim'
    Plug 'tpope/vim-fugitive'
    " Plug 'alvan/vim-closetag'

    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    Plug 'nvim-treesitter/nvim-treesitter-refactor'
    " Plug 'jiangmiao/auto-pairs'
    Plug 'cohama/lexima.vim'
    Plug 'hrsh7th/nvim-compe'
    Plug 'neovim/nvim-lspconfig'
    " Plug 'tzachar/compe-tabnine', { 'do': './install.sh'  }
    Plug 'psliwka/vim-smoothie'

    " Plug 'junegunn/goyo.vim'
    Plug 'tpope/vim-commentary'
    Plug 'SirVer/ultisnips'
    Plug 'Yggdroot/indentLine'
    Plug 'puremourning/vimspector'
    " Plug 'rust-lang/rust.vim'
    " Plug 'racer-rust/vim-racer'
    " Plug 'dylon/vim-antlr'

    " Plug 'heavenshell/vim-pydocstring', { 'do': 'make install' }
    Plug 'itchyny/vim-cursorword'

    Plug 'dbakker/vim-projectroot'
    Plug 'onsails/lspkind-nvim'
    Plug 'phaazon/hop.nvim'
    " Plug 'kosayoda/nvim-lightbulb'

    Plug 'michaelb/sniprun', {'do': 'bash install.sh'}
call plug#end()

set completeopt=menuone,noselect
set clipboard+=unnamedplus
let g:clipboard = {
          \   'name': 'win32yank-wsl',
          \   'copy': {
          \      '+': 'win32yank.exe -i --crlf',
          \      '*': 'win32yank.exe -i --crlf',
          \    },
          \   'paste': {
          \      '+': 'win32yank.exe -o --lf',
          \      '*': 'win32yank.exe -o --lf',
          \   },
          \   'cache_enabled': 0,
          \ }


set nocompatible              " be iMproved, required
filetype plugin indent on    " required


runtime ./settings.vim
lua require 'init'


function s:LightlineCurrentFunctionVista() abort
  let l:method = get(b:, 'vista_nearest_method_or_function', '')
  if l:method != ''
    let l:method = '[' . l:method . ']'
    echo l:method
  endif
  echo 'lol'
endfunction
" au VimEnter * call vista#RunForNearestMethodOrFunction()

set shell=zsh

set background=dark
set listchars=trail:·,tab:->,nbsp:·,space:·
set list

set expandtab "ставим табы пробелами
set cursorline

se nobackup
se noundofile


set tabstop=2
set shiftwidth=2
set smarttab
set softtabstop=2 "2 пробела в табе

" set autoindent
set wmnu

let maplocalleader = ","


set termguicolors
set nu "Включаем нумерацию строк
set mousehide "Спрятать курсор мыши когда набираем текст
set mouse=a "Включить поддержку мыши 
set termencoding=utf-8 "Кодировка терминала
set novisualbell "Не мигать
" Удобное поведение backspace
set backspace=indent,eol,start whichwrap+=<,>,[,]
" Вырубаем черточки на табах
set showtabline=1

" Переносим на другую строчку, разрываем строки
set wrap
set linebreak

set encoding=utf-8 " Кодировка файлов по умолчанию
set fileencodings=utf8

set ruler

set hidden

let &showbreak = '↵'
set bri

set shell=/usr/bin/zsh

" Выключаем звук в Vim
set visualbell t_vb=
" set guifont = JetBrains\ Mono\ Nerd\ Font\ Mono:h13
"       "fontFace": "JetBrainsMono Nerd Font Mono",
set smartindent

function! <SID>AutoProjectRootCD()
  try
    if &ft != 'help'
      ProjectRootCD
    endif
  catch
    " Silently ignore invalid buffers
  endtry
endfunction

" augroup Yank
  " autocmd!
  " autocmd TextYankPost * :call system('clip.exe ',@")
" augroup END

augroup Compilers
  au BufEnter *.c let b:current_compiler= 'gcc -o ' . expand('%:t:r')
augroup END

autocmd BufEnter * call <SID>AutoProjectRootCD()

" au CursorHold * call s:LightlineCurrentFunctionVista()
" au CursorMoved * call s:LightlineCurrentFunctionVista()
autocmd VimEnter * nested :Vista!

" autocmd CursorHold,CursorHoldI * lua require'nvim-lightbulb'.update_lightbulb()
" Перед сохранением вырезаем пробелы на концах (только в .py файлах)
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
" В .py файлах включаем умные отступы после ключевых слов
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

autocmd FileType python runtime ./python.vim

set t_Co=256

colorscheme codedark
runtime ./key.vim
runtime ./theme.vim
