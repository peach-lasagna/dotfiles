function s:create_basic_map(key, command)
    nnoremap key         command<CR>
    vnoremap key         <C-C>command<CR>
    inoremap key         <C-O>command<CR>
endfunction

nnoremap <silent> <C-S>              :update<CR>
vnoremap <silent> <C-S>   /      <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>

nnoremap gd  :lua vim.lsp.buf.definition()<cr>
nnoremap ggd gd
nnoremap gr :lua vim.lsp.buf.references()<cr>
nnoremap <C-e> :lua vim.lsp.buf.signature_help()<cr>
inoremap <C-e> <C-O>:lua vim.lsp.buf.signature_help()<cr>
nmap <leader>l :lua vim.lsp.buf.

nmap <localleader>w :HopWord<cr>
nmap <localleader>l :HopLine<cr>
imap <silent> <leader>l <C-O>:HopLine<cr>
imap <silent> <leader>s <C-O>:HopWord<cr>

vnoremap ss <C-c>:'<,'>SnipRun<cr>
nmap ss :SnipRun<cr>

nnoremap <leader>q  :BufferClose<CR>
nnoremap <leader>Q  :q<cr>
nnoremap <leader>qq :q<cr>:BufferClose<cr>

nnoremap <C-l> <C-l>:noh<CR>

nnoremap W <C-w>
inoremap <leader>w <C-O><c-w>

nnoremap <silent> <C-a> :NvimTreeToggle<CR>
inoremap <silent> <C-a> <esc>:NvimTreeToggle<CR>

nnoremap <silent> <c-z> :u<CR>
inoremap <silent> <c-z> <C-O>:u<CR>

inoremap <silent> <leader>] <C-O>>>
inoremap <silent> <leader>[ <C-O><<
inoremap <silent> <leader>4 <C-O>$
inoremap <silent> <leader>5 <C-O>%
inoremap <silent> <leader>d <C-O>dd
inoremap <silent> <leader>O <C-O>O
inoremap <silent> <leader>o <C-O>o
inoremap jk <esc>

" nmap <esc> i

nnoremap <C-S-D> :%d
nnoremap <C-S-a> :%y+

imap <silent> <leader>p <C-O>p
nnoremap <silent> <C-s> :w<CR>
inoremap <silent> <C-s> <C-O>:w<CR>

inoremap <C-j> <C-O>:set relativenumber!<CR>
nnoremap <C-j> :set relativenumber!<CR>

nnoremap <C-H>         db
" vnoremap <C-H>         <C-c>db
inoremap <C-H>         <esc>dbdli

nnoremap <C-BS>         db
" vnoremap <C-BS>         <C-c>db
inoremap <C-BS>         <esc>dbdli

nnoremap <C-Del>         dw
vnoremap <C-Del>         <C-C>dw
inoremap <C-Del>         <C-O>dw

inoremap <localleader>k <Esc>
nnoremap <localleader>k i

nnoremap <C-`>       :Goyo!<CR>
inoremap <C-`>       <C-O>:Goyo!<CR>

" nnoremap <C-LeftMouse> gf

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

inoremap %% <C-O>%

" nnoremap <RightMouse> <Nop>
" nnoremap <silent> <RightDrag> <Cmd>lua require("gesture").draw()<CR>
" nnoremap <silent> <RightRelease> <Cmd>lua require("gesture").finish()<CR>

imap <A-c> <C-O>gcc
nmap <A-c> gcc
vmap <A-c> gc

vmap <C-c> y
nmap <C-v> p
imap <C-v> <C-o>p
" map "+p :r !powershell.exe -Command Get-Clipboard<CR>

nnoremap <S-C-M> k
nnoremap <F6> :exe ':!' . b:current_compiler . ' ' . @%<cr>

nnoremap <F2> :BufferPrevious<CR>
nnoremap <F3> :BufferNext<CR>
inoremap <F2> <C-O>:BufferPrevious<CR>
inoremap <F3> <C-O>:BufferNext<CR>

nnoremap <leader>dp :ProjectRootCD<cr>

nmap <C-\> :Vista!!<CR>
inoremap <C-\> <C-O>:Vista!!<CR>

"vimspector
nnoremap <localleader>gd :call vimspector#Launch()<cr>
nnoremap <F5> :call vimspector#Continue()<cr>
nnoremap <localleader>gs :call vimspector#Stop()<cr>
nnoremap <localleader>gR :call vimspector#Restart()<cr>
nnoremap <localleader>gp :call vimspector#Pause()<cr>
nnoremap <localleader>gb :call vimspector#ToggleBreakpoint()<cr>
nnoremap <localleader>gB :call vimspector#ToggleConditionalBreakpoint()<cr>
nnoremap <localleader>gn :call vimspector#StepOver()<cr>
nnoremap <localleader>gi :call vimspector#StepInto()<cr>
nnoremap <localleader>go :call vimspector#StepOut()<cr>
nnoremap <localleader>gr :call vimspector#RunToCursor()<cr>


inoremap <silent><expr> <C-Space> compe#complete()
inoremap <silent><expr> <CR>      compe#confirm(lexima#expand('<LT>CR>', 'i'))
inoremap <silent><expr> <C-e>     compe#close('<C-e>')
inoremap <silent><expr> <C-f>     compe#scroll({ 'delta': +4  })
inoremap <silent><expr> <C-d>     compe#scroll({ 'delta': -4  })

se pastetoggle=<F12>

" command! Vb normal! <C-v>
com PI PlugInstall
com PC PlugClean
com PU PlugUpdate
com X xa
com H checkhealth

