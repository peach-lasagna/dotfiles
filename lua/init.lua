require 'darkline'
require 'lsp'
require 'complete'
vim.o.guifont = "JetBrainsMono Nerd Font:h12"


require'hop'.setup { keys = 'qweasdzxcrft' }

require'sniprun'.setup({
    selected_interpreters={'Python3_jupyter', 'Lua_nvim', 'C_original', "Rust_original"}
})

require'nvim-treesitter.configs'.setup {
  refactor = {
    highlight_definitions = { enable = true },
    highlight_current_scope = { enable = true },
  },
  highlight = {
    enable = true
  }
}
local tree_cb = require'nvim-tree.config'.nvim_tree_callback
vim.g.nvim_tree_bindings = {
  {key = "<C-e>", cb = tree_cb("vsplit")},
}

-- local gesture = require('gesture')
-- gesture.register({
--   name = "scroll to bottom",
--   inputs = { gesture.up(), gesture.down() },
--   action = "normal! G"
-- })
-- gesture.register({
--   name = "next tab",
--   inputs = { gesture.right() },
--   action = "tabnext"
-- })
-- gesture.register({
--   name = "previous tab",
--   inputs = { gesture.left() },
--   action = function() -- also can use function
--     vim.cmd("tabprevious")
--   end,
-- })
-- gesture.register({
--   name = "go back",
--   inputs = { gesture.right(), gesture.left() },
--   -- map to `<C-o>` keycode
--   action = [[lua vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-o>", true, false, true), "n", true)]]
-- })


require('lspkind').init({
  with_text = true,
  symbol_map = {
    Text = '',
    Method = 'ƒ',
    Function = '',
    Constructor = '',
    Variable = '',
    Class = '',
    Interface = 'ﰮ',
    Module = '',
    Property = '',
    Unit = '',
    Value = '',
    Enum = '了',
    Keyword = '',
    Snippet = '﬌',
    Color = '',
    File = '',
    Folder = '',
    EnumMember = '',
    Constant = '',
    Struct = ''
  },
})

