
local ts_utils = require'nvim-treesitter.ts_utils'
local locals = require'nvim-treesitter.locals'
local api = vim.api
local cmd = api.nvim_command

local M = {}

local current_scope_namespace = api.nvim_create_namespace('nvim-treesitter-current-scope')

function M.highlight_current_scope(bufnr)
  M.clear_highlights(bufnr)

  local node_at_point = ts_utils.get_node_at_cursor()
  local current_scope = locals.containing_scope(node_at_point, bufnr)

  if current_scope then
    local start_line = current_scope:start()

    if start_line ~= 0 then
      ts_utils.highlight_node(current_scope, bufnr, current_scope_namespace, 'TSCurrentScope')
    end
  end
end

function M.clear_highlights(bufnr)
  api.nvim_buf_clear_namespace(bufnr, current_scope_namespace, 0, -1)
end

return M
