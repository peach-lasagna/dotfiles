let g:hello_nvim_path = '/mnt/c/Users/kir/Opens/radare2/doc/fortunes.fun'

" lexima
let g:lexima_no_default_rules = v:true
call lexima#set_default_rules()
" let g:lexima_my = [
"   \ {'char': ':', 'input_before': ':', 'filetype': 'rust'}, 
"   \ {'char': '<BS>', 'at': '::\%#', 'delete':1, 'filetype': 'rust'},
"   \ {'char': '|', 'input_after': '|', 'filetype': 'rust'}, 
"   \ {'char': '<BS>', 'at': '|\%#|', 'delete':1, 'filetype': 'rust'},
"   \ {'char': '/*', 'input_after': '*/', 'filetype': 'rust'}, 
"   \ {'char': '<', 'input_after': '>', 'filetype': 'rust'}, 
"   \ {'char': '<BS>', 'at': '/*\%#*/', 'delete':1, 'filetype': 'rust'},
"   \ {'char': '<BS>', 'at': '<\%#>', 'delete':1, 'filetype': 'rust'},
"   \ {'char': '<Space>', 'at': '/*\%#*/', 'input_after': '<Space>', 'filetype': 'rust'},
"   \ {'char': '<BS>', 'at': '/* \%# */', 'delete': 1, 'filetype': 'rust'},
"   \ ]

" let g:lexima_my = [
"   \ {'char': '|', 'input_after': '|', 'filetype': 'rust'},
"   \ ]
" for rule in g:lexima_my
"   call lexima#add_rule(rule)
" endfor

"tree
let g:nvim_tree_auto_open = 1
let g:nvim_tree_tab_open = 1
let g:nvim_tree_gitignore = 1
let g:nvim_tree_ignore = [
  \ "node_modules",
  \ "__pycache__",
  \ ".venv",
  \ ".git",
  \ ".venv",
  \ "target",
  \ "Cargo.lock"
  \ ]

let g:nvim_tree_icons = {
    \ 'default': '',
    \ 'symlink': '',
    \ 'git': {
    \   'unstaged':  "✗",
    \   'staged':    "ST",
    \   'unmerged':  "",
    \   'renamed':   "➜",
    \   'untracked': "UU",
    \   'deleted':   "DD",
    \   'ignored':   "II"
    \   },
    \ 'folder': {
    \   'default': "",
    \   'open': "",
    \   'empty': "",
    \   'empty_open': "",
    \   'symlink': "",
    \   'symlink_open': "",
    \   }
    \ }


let g:pydocstring_formatter = 'google'


" Ultisnip
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"  "<c-b>
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"gitgutter
let g:gitgutter_sign_removed = '-'
let g:gitgutter_sign_added = '+'
let g:gitgutter_sign_modified = '~'

" echodoc
" let g:echodoc#enable_at_startup = 1
" let g:echodoc#type = 'floating'

"vista
let g:vista_icon_indent = ["|>", "-->"]

"indent
" let g:indentLine_setColors = 0
let g:indentLine_char_list = ['┊']
let g:indentLine_defaultGroup = 'SpecialKey'
