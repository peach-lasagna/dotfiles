"hi ColorColumn ctermbg=125
"hi link EchoDocFloat Pmenu
hi link CompeDocumentation               NormalFloat

hi link LspDiagnosticsVirtualTextError   Error
hi link LspDiagnosticsSignError          gitcommitDiscardedType
hi link LspDiagnosticsUnderlineError     Error

hi link LspDiagnosticsVirtualTextWarning WarningMsg
hi link LspDiagnosticsSignWarning        WarningMsg
hi link LspDiagnosticsUnderlineWarning   WarningMsg

hi link LspDiagnosticsUnderlineInformation GalaxyDiagnosticInfo
hi link LspDiagnosticsVirtualTextInformation GalaxyDiagnosticInfo
hi link LspDiagnosticsSignInformation    GalaxyDiagnosticInfo

hi link Todo                             gitcommitDiscardedType
hi link pythonTodo                       gitcommitDiscardedType

hi link LspDiagnosticsVirtualTextHint    Float
hi link LspDiagnosticsSignHint           Float
hi link LspDiagnosticsUnderlineHint      Float

hi! GitGutterAdd guifg=#98be65 guibg=#202328    "GalaxyDiffAdd
hi! GitGutterChange guifg=#FF8800 guibg=#202328 "GalaxyDiffModified
hi! GitGutterDelete guifg=#ec5f67 guibg=#202328 "GalaxyDiffRemoved

hi! CursorLine guibg=#202328
hi! ColorColumn guibg=#202328
hi! LspReferenceText guifg=#ec5f67 guibg=#202328 "PmenuSbar
hi! LspReferenceWrite guifg=#ec5f67 guibg=#202328 "PmenuSbar
hi! LspReferenceRead guifg=#ec5f67 guibg=#202328 "PmenuSbar

hi! BufferInactiveSign guifg=#98be65 guibg=#202328
" hi! BufferInactive guifg=#B5CEA8

hi! BufferCurrent ctermfg=117 guifg=#9CDCFE

hi CurrentWord guifg=#ec5f67
hi CurrentWordTwins guifg=#ec5f67

" hi! link TSCurrentScope SpecialComment

sign define LspDiagnosticsSignInformation  text=    numhl=LspDiagnosticsSignInformation
sign define LspDiagnosticsSignWarning      text=    numhl=LspDiagnosticsSignWarning
sign define LspDiagnosticsSignError text=✗    numhl=LspDiagnosticsSignError
sign define LspDiagnosticsSignHint text=﬌    numhl=LspDiagnosticsSignHint

hi! LineHigh guifg=#FF8800
sign define LineScope text=▎ texthl=LineHigh
