function gvp {
    gaa
    gcmsg "$1"
    ggp
}
function ghres {
    git reset --hard HEAD~$1
    git merge --squash HEAD@{1}
    git commit
}
alias e="explorer.exe ."
alias cw="cargo build --target x86_64-pc-windows-gnu"
alias crw="cargo run  --target x86_64-pc-windows-gnu"
alias ch="cargo check"
alias ree="rustc --explain"
alias ce="cargo expand"
alias cn="cargo new"

alias ida="/mnt/c/Users/kir/Tools/IDA/ida.exe"
alias ida64="/mnt/c/Users/kir/Tools/IDA/ida64.exe"

alias ext="youtube-dl --extract-audio --audio-format mp3 "
# alias grun='java -Xmx500M -cp "/usr/local/lib/antlr-4.9-complete.jar:$CLASSPATH" org.antlr.v4.gui.TestRig'
# alias antlr4='java -Xmx500M -cp "/usr/local/lib/antlr-4.9-complete.jar:$CLASSPATH" org.antlr.v4.Tool'

alias ghs="git hist"
alias javaw="java.exe"
# alias dirb="javaw -jar '/mnt/c/Users/kir/Tools/DirBuster/DirBuster.jar'"
alias burp="javaw -javaagent:'C:\\Users\\kir\\Opens\\burp\\BurpSuiteLoader_v2021.4.1.jar' -noverify -jar 'C:\\Users\\kir\\Opens\\burp\\burpsuite_pro.jar' &"
alias ti="touch __init__.py"
alias lite="/mnt/c/Users/kir/Opens/lite/lite.exe"

alias nv="/mnt/c/Users/kir/Tools/Neovim/bin/neovide.exe --wsl"
alias fvim="/mnt/c/Users/kir/Tools/Neovim/fvim/FVim.exe --wsl"

alias py="python3.9"
alias vim="nvim"
alias pym="py -m"
alias v="nvim"
alias ccat='pygmentize -g'
alias ipy="ipython"
alias ipyc="ctf && ipython --profile=ctf"
alias pu="py -m poetry update"
alias sqlmap="py /mnt/c/Users/kir/Tools/sqlmap-dev/sqlmap.py --threads=4"
alias gdb="gdb -q"
alias gdbs="gdbserver localhost:23946"
alias ctf="source /mnt/c/Users/kir/Tools/RsaCtfTool/.venv/bin/activate"

function ve {
  source .venv/bin/activate || ( ( pym venv .venv ) && source .venv/bin/activate)
}

alias p='/mnt/c/Users/kir/Projects'
alias to='/mnt/c/Users/kir/Tools'
alias re='/mnt/c/Users/kir/Downloads/reverse'
alias zrc="v ~/.scripts/* ~/.zshrc "
alias brc="v ~/.bashrc"
# alias vrc="vim ~/.vimrc"
# alias nvrc="nvim ~/.config/nvim/init.vim"
alias nvc="v ~/.config/nvim/init.vim ~/.config/nvim/lua/init.lua ~/.config/nvim/*.* "

alias psq="psql -U postgres"
alias ghome="/mnt/c/Users/kir"

alias poetry="py -m poetry"
alias pos="py -m poetry shell"
alias pa="poetry add"

alias uvi="uvicorn app.main:app --reload"
alias sai="sudo apt install"
alias saa="sudo apt update && sudo apt upgrade -y"
alias sar="sudo apt-get autoremove"

alias pip="pym pip"
alias pfr="pip freeze > requirements.txt"
alias pipr="pip install -r requirements.txt"
alias pi="pip install"
alias cookie="pym cookiecutter https://github.com/peach-lasagna/cookie_classic.git"
alias cl="git clone"

alias t="touch"
